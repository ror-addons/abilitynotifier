<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="AbilityNotifier" version="1.0" date="25/09/2008" >

		<Author name="Deakster" email="deakster@gmail.com" />
		<Description text="Reminds/notifies you to use skills." />
		
		<Dependencies>
			<Dependency name="LibSlash" />
	    </Dependencies>
        
		<Files>
			<File name="AbilityNotifier.xml" />
		</Files>
		
		<OnInitialize>
			<CallFunction name="AbHelp.Initialize" />
		</OnInitialize>
		<OnUpdate/>
		<OnShutdown/>
		
		<SavedVariables>
			<SavedVariable name="AbHelpData" />
		</SavedVariables>
	</UiMod>
</ModuleFile>
