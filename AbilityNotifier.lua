AbHelp = {}
AbHelpData = {Parry = false, Block = false, Disrupt = false, CombatBuff = false, CombatBuffList = {}}

function AbHelp.Initialize()
	CreateWindow("AbHelpWindow", true)
	LayoutEditor.RegisterWindow("AbHelpWindow", L"Ability Notifier", L"Reminds you to use skills.", false, false, true, nil)
	RegisterEventHandler(SystemData.Events.WORLD_OBJ_COMBAT_EVENT, "AbHelp.CombatEvent")
	RegisterEventHandler(SystemData.Events.PLAYER_COMBAT_FLAG_UPDATED, "AbHelp.CombatUpdated")
	
	-- Handle commands
	local slashhandler = function(what)
		local handlers = {
			parry = function()
				AbHelpData.Parry = not AbHelpData.Parry
				AbHelp.Echo("Parry notifications switched [" .. AbHelp.BoolToSwitch(AbHelpData.Parry) .. "].")
			end,
			
			block = function()
				AbHelpData.Block = not AbHelpData.Block
				AbHelp.Echo("Block notifications switched [" .. AbHelp.BoolToSwitch(AbHelpData.Block) .. "].")
			end,
			
			disrupt = function()
				AbHelpData.Disrupt = not AbHelpData.Disrupt
				AbHelp.Echo("Disrupt notifications switched [" .. AbHelp.BoolToSwitch(AbHelpData.Disrupt) .. "].")
			end,
			
			combatbuff = function(rest)
				if rest == "" then
					AbHelpData.CombatBuff = not AbHelpData.CombatBuff
					AbHelp.Echo("Combat buff notifications switched [" .. AbHelp.BoolToSwitch(AbHelpData.CombatBuff) .. "].")
				else
					local count = 0
					AbHelpData.CombatBuffList = {}
					for i, val in pairs(AbHelp.Split(rest, ",")) do
						AbHelpData.CombatBuffList[#AbHelpData.CombatBuffList+1] = AbHelp.Trim(val)
						count = count + 1
					end
					AbHelp.Echo("Added " .. count .. " buffs to combat buff notify list.")
				end
			end
		}
		
		comm, params = string.match(what, "^%s*(%w*)%s*(.-)%s*$")
		comm = string.lower(comm)
		
		-- Display help if nothing was passed
		if comm == "" then
			AbHelp.Echo("-- Parry notifications are [" .. AbHelp.BoolToSwitch(AbHelpData.Parry) ..
				"], block notifications are [" .. AbHelp.BoolToSwitch(AbHelpData.Block) ..
				"], disrupt notifications are [" .. AbHelp.BoolToSwitch(AbHelpData.Disrupt) ..
				"], combat buff notifications are [" .. AbHelp.BoolToSwitch(AbHelpData.CombatBuff) .. "]")
			if #AbHelpData.CombatBuffList > 0 then
				AbHelp.Echo("-- Combat buffs are: " .. table.concat(AbHelpData.CombatBuffList, ","))
			else
				AbHelp.Echo("-- You have no combat buffs set.")
			end
			AbHelp.Echo("-- To switch on or off: /an parry, /an block, /an disrupt, /an combatbuff")
			AbHelp.Echo("-- To add combat buffs: /an combatbuff [comma separated list of buff name contents]")
			return
		end
		
		if handlers[comm] == nil then
			AbHelp.Echo("'" .. comm .. "' is not a command.")
			return
		end
		
		handlers[comm](params)
	end
	
	LibSlash.RegisterSlashCmd("an", slashhandler)
	LibSlash.RegisterSlashCmd("anot", slashhandler)
end

function AbHelp.Notify(what)
	LabelSetText("AbHelpText", StringToWString(what))
	WindowStartAlphaAnimation("AbHelpWindow",
		Window.AnimationType.EASE_OUT, 1, 0, 2, false, 0, 1)
end

function AbHelp.CombatEvent(target, hitAmount, hittype)
	if AbHelpData.Parry and target == GameData.Player.worldObjNum and hittype == GameData.CombatEvent.PARRY then
		AbHelp.Notify("PARRY")
		return
	end
	
	if AbHelpData.Block and target == GameData.Player.worldObjNum and hittype == GameData.CombatEvent.BLOCK then
		AbHelp.Notify("BLOCK")
		return
	end
	
	if AbHelpData.Disrupt and target == GameData.Player.worldObjNum and hittype == GameData.CombatEvent.DISRUPT then
		AbHelp.Notify("DISRUPT")
		return
	end
end

function AbHelp.CombatUpdated(...)
	if AbHelpData.CombatBuff and #arg > 0 and arg[1] then
		for i, val in pairs(AbHelpData.CombatBuffList) do
			local found = false
			for i, buff in pairs(GetBuffs(GameData.BuffTargetType.SELF)) do
				if string.find(string.lower(WStringToString(buff.name)), string.lower(val)) ~= nil then found = true end
			end
			if not found then AbHelp.Notify(val) end
		end
	end
end

function AbHelp.Echo(what)
	EA_ChatWindow.Print(StringToWString(tostring(what)))
end

function AbHelp.BoolToSwitch(what)
	if what then return "on" else return "off" end
end

function AbHelp:Split(delimiter)
	local result = { }
	local from  = 1
	local delim_from, delim_to = string.find( self, delimiter, from  )
	while delim_from do
		table.insert( result, string.sub( self, from , delim_from-1 ) )
		from  = delim_to + 1
		delim_from, delim_to = string.find( self, delimiter, from  )
	end
	table.insert( result, string.sub( self, from  ) )
	return result
end

function AbHelp.Trim(what)
	return string.gsub(what, "^%s*(.-)%s*$", "%1")
end
