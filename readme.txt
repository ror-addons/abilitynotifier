
Ability Notifier v0.7

    Author: deakster@gmail.com 
    Requires: LibSlash 

Ability Notifier reminds you to use certain skills under certain conditions using a large, clear notification area. For the moment, the mod can notify you when:

    * You Parry (to remind you to cast skills that rely on having just parried).
    * You Block (to remind you to cast skills that rely on having just blocked).
    * You enter combat without certain specified buffs on yourself (if you forget to cast or recast certain long lasting buffs on yourself).
    * You Disrupt 

All notification are optional, and can be toggled based on your needs.

Note that the combat buff notifier can take partial buff names, so adding 'Blessed Bullets' as a required combat buff will match any of the 3 bullet buffs.

If there are any other situations that you think would benefit from a reminder, then please let me know (does any class benefit from a reminder if they evade? if their target evades? etc).

Usage:

The notification area can be moved, resized and hidden using the built in layout editor.

NOTE: If /an doesn't work for you, you can use /anot

For help and info:

    /an 

To toggle the various notifications on or off:

    /an parry, /an block, /an disrupt, /an combatbuff 

To set your combat buff list:

    /an combatbuff [comma separated list of buff names or partial buff names] 
    example: /an combatbuff Blessed Bullets, Buff2, Buff3 

